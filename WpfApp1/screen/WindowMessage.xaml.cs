﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using WpfApp1.helper.factory;
using WpfApp1.helper.generator;
using WpfApp1.model;
using WpfApp1.vue_model;

namespace WpfApp1
{
    /// <summary>
    /// Interaction logic for WindowMessage.xaml
    /// </summary>
    public partial class WindowMessage : Window
    {
        ObservableCollection<Message> messages;
        ObservableCollection<Personne> personnes;

        MainVueModel mainVueModel;

        Personne receiver;

        public WindowMessage()
        {
            InitializeComponent();
            this.DataContext = this;

            mainVueModel = new MainVueModel();

            this.personnes = new ObservableCollection<Personne>(mainVueModel.contactList);
            selectReceiver(this.personnes.First());

        }

        public ObservableCollection<Personne> personneList
        {
            get
            {
                return personnes;
            }
        }

        public ObservableCollection<Message> messageList
        {
            get
            {
                return messages;
            }
        }

        public void selectReceiver(Personne receiver)
        {
            this.receiver = receiver;
            this.messages = new ObservableCollection<Message>(mainVueModel.getConversationWith(receiver.id));
        }

        void handleSendBtnClick(object sender, RoutedEventArgs e)
        {
            if (receiver == null) return;

            // Get the current message from TextBox
            string newText = this.messageTextBox.Text;


            List<Message> newMessageList = mainVueModel.sendMessage(receiver, newText);
            
            messages = new ObservableCollection<Message>(newMessageList);

            // Clear the TextBox
            this.messageTextBox.Clear();
        }

    }
}
