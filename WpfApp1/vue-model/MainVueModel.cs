﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.helper.factory;
using WpfApp1.helper.generator;
using WpfApp1.model;
using WpfApp1.service;

namespace WpfApp1.vue_model
{
    public class MainVueModel {

        // services
        private AuthService authService;
        private ConversationService conversationService;

        // Personnes
        List<Personne> personneList;

        public MainVueModel()
        {
            authService = (new AuthService()).getInstance();
            conversationService = (new ConversationService()).getInstance();

            generateFakeConversation();
        }

        public List<Message> getConversationWith(string personneId)
        {
            return conversationService.getMessagesWith(personneId);
        }

        public List<Message> sendMessage(Personne receiver, string text)
        {
            conversationService.sendMesssage(receiver, text);
            return getConversationWith(receiver.id);
        }

        public List<Personne> contactList
        {
            get
            {
                return personneList;
            }
        }


        private void generateFakeConversation()
        {
            Personne user = authService.connect("test", "test");

            personneList = PersonneGenerator.getPersonnes(6);

            for(int i = 0; i < personneList.Count(); i++)
            {
                Personne to = personneList.ElementAt(i);

                for(int j = 0; j < 3; j++)
                {
                    string text = MessageGenerator.getText();
                    sendMessage(to, text);
                }
            }
        }
    }
}
