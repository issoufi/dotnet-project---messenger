﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.helper.factory;
using WpfApp1.helper.generator;
using WpfApp1.model;

namespace WpfApp1.service
{
    public class ConversationService
    {
        private ConversationService singleInstance = null;

        private Dictionary<string, List<Message>> conversationList;

        // Factorys
        private MessageFactory messageFactory;

        // services
        private AuthService authService;

        public ConversationService()
        {
            messageFactory = (new MessageFactory()).getInstance();
            authService = (new AuthService()).getInstance();
            conversationList = new Dictionary<string, List<Message>>();
        }

        public void sendMesssage(Personne to, string message)
        {
            Personne from = authService.currentUser;
            string fromId = from.id;
            Message msg = messageFactory.create(from, to, message);

            List<Message> conv;

            if (conversationList.ContainsKey(fromId))
            {
                conv = conversationList[fromId];
                conv.Add(msg);
                conversationList[fromId] = conv;

            } else
            {
                conv = new List<Message>();
                conv.Add(msg);
                conversationList.Add(fromId, conv);
            }
        }

        public List<Message> getMessagesWith(string userId)
        {
            List<Message> conv;

            if (conversationList.ContainsKey(userId))
            {
                conv = conversationList[userId];

            }
            else
            {
                conv = new List<Message>();
            }

            return conv;
        }



        public ConversationService getInstance()
        {
            if (singleInstance == null)
            {
                singleInstance = new ConversationService();
            }

            return singleInstance;
        }
    }
}
