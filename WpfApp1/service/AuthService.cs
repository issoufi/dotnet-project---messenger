﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.helper.factory;
using WpfApp1.helper.generator;
using WpfApp1.model;

namespace WpfApp1.service
{
    public class AuthService
    {
        private static Personne _currentUser;
        private AuthService singleInstance = null;

        public Personne connect(string username, string password)
        {
            _currentUser = PersonneGenerator.getPersonne();
            return currentUser;
        }

        public void disconnect()
        {
            _currentUser = null;
        }

        public Personne currentUser
        {
            get
            {
                return _currentUser;
            }
        }

        public AuthService getInstance()
        {
            if (singleInstance == null)
            {
                singleInstance = new AuthService();
            }

            return singleInstance;
        }
    }
}
