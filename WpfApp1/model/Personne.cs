﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.model
{
    public class Personne
    {
        string _id, firstname, lastname;
        DateTime date;
        List<Message> conversationList;

        public Personne(string id, string firstname, string lastname, DateTime date)
        {
            this._id = id;
            this.firstname = firstname;
            this.lastname = lastname;
            this.date = date;
            this.conversationList = new List<Message>();
        }

        public string id
        {
            get
            {
                return _id;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} {1}", this.firstname, this.lastname);
        }
    }
}
