﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfApp1.model
{
    public class Message
    {
        string _id, _message;
        DateTime _date;
        Personne _from, _to;

        public Message(string id, Personne from, Personne to, string message, DateTime date)
        {
            this._id = id;
            this._from = from;
            this._to = to;
            this._message = message;
            this._date = date;
        }

        public override string ToString()
        {
            return this._message;
        }

        public Personne to {
            get
            {
                return _to;
            }
        }

        public string message
        {
            get
            {
                return _message;
            }
        }

        
    }
}
