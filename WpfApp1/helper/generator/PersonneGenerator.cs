﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.helper.factory;
using WpfApp1.model;

namespace WpfApp1.helper.generator
{
    public static class PersonneGenerator
    {
        private static string[] nameList =
 {
            "Arden",
            "Baumgartner",
            "Queenie",
            "Brunett",
            "Reina",
            "Brakebill",
            "Magdalene",
            "Start",
            "Sage",
            "Klassen",
            "Danica",
            "Pauly",
            "Alva",
            "Lamberti",
            "Zella",
            "Simons",
            "Marcus",
            "Biggers",
            "Caroline",
            "Borba",
            "Delena",
            "Yi",
            "Kazuko",
            "Mehan",
            "Leda",
            "Montrose",
            "Estela",
            "Mcnulty",
            "Pricilla",
            "Tokar",
            "Hortensia",
            "Vangieson",
            "Marget",
            "Tsan",
            "Angle",
            "Kania",
            "Penny",
            "Shutts",
            "Germaine",
            "Drayer"
        };

        private static Random rdm = new Random();

        public static Personne getPersonne()
        {
            
            int firstNameIndex = rdm.Next(0, nameList.Count());
            int lastNameIndex = rdm.Next(0, nameList.Count());

            string firstname = nameList.ElementAt(firstNameIndex);
            string lastName = nameList.ElementAt(lastNameIndex);

            return (new PersonneFactory()).getInstance().create(firstname, lastName);
        }

        public static List<Personne> getPersonnes(int nbPeronne)
        {
            List<Personne> result = new List<Personne>();

            for (int i = 0; i < nbPeronne; i++)
            {
                result.Add(getPersonne());
            }

            return result;
        }
    }
}
