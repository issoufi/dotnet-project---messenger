﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.helper.factory;
using WpfApp1.model;

namespace WpfApp1.helper.generator
{
    public static class MessageGenerator
    {
        private static string[] messageList =
        {
            "Sense child do state to defer mr of forty.",
            "Become latter but nor abroad wisdom waited. Was delivered gentleman acuteness but daughters",
            "In as of whole as match asked. Pleasure exertion put add entrance distance drawings.",
            "In equally matters showing greatly it as",
            "Want name any wise are able park when.",
            "Saw vicinity judgment remember finished men throwing.",
            "Extended kindness trifling remember he confined outlived if",
            "Assistance sentiments yet unpleasing say.",
            "Open they an busy they my such high.",
            "An active dinner wishes at unable hardly no talked on",
            "Immediate him her resolving his favourite.",
            "Wished denote abroad at branch at.",
            "Able an hope of body. Any nay shyness article matters own removal nothing his forming.",
            "Gay own additions education satisfied the perpetual.",
            "If he cause manor happy. Without farther she exposed saw man led.",
            "Along on happy could cease green oh.",
            "Far curiosity incommode now led smallness allowance.",
            "Favour bed assure son things yet.",
            "She consisted consulted elsewhere happiness disposing household any old the.",
            "Widow downs you new shade drift hopes small."
        };

        private static Random rdm = new Random();

        public static string getText()
        {
            int index = rdm.Next(0, messageList.Count());
            string message = messageList.ElementAt(index);

            return message;
        }

        public static Message getMessage(Personne from)
        {

            string text = getText();
            Personne to = PersonneGenerator.getPersonne();

            return (new MessageFactory()).getInstance().create(from, to, text);
        }

        public static List<Message> getMessages(Personne from, int nbMessage)
        {
            List<Message> result = new List<Message>();

            for(int i = 0; i < nbMessage; i++)
            {
                result.Add(getMessage(from));
            }

            return result;
        }
    }
}
