﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.model;

namespace WpfApp1.helper.factory
{
    public class PersonneFactory
    {
        public static int lastUsedId = 0;
        private PersonneFactory singleInstance = null;


        public Personne create(string firstname, string lastname)
        {
            return new Personne((PersonneFactory.lastUsedId++).ToString(), firstname, lastname, new DateTime());

        }

        public PersonneFactory getInstance()
        {
            if (singleInstance == null)
            {
                singleInstance = new PersonneFactory();
            }

            return singleInstance;
        }

    }
}
