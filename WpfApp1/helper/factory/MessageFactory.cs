﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WpfApp1.model;

namespace WpfApp1.helper.factory
{
    public class MessageFactory
    {
        public static int lastUsedId = 0;
        private MessageFactory singleInstance = null;


        public Message create(Personne from, Personne to, string message)
        {
            return new Message((MessageFactory.lastUsedId++).ToString(), from, to, message, new DateTime());

        }

        public MessageFactory getInstance()
        {
            if (singleInstance == null)
            {
                singleInstance = new MessageFactory();
            }

            return singleInstance;
        }
    }
}
